import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { SnakeModule } from './snake/snake.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent],
  imports: [NgbModule, BrowserModule, SnakeModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
