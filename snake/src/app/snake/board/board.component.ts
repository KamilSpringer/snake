import { Component, OnInit, HostListener } from '@angular/core';
import { Position } from '../../position';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NewGameModalComponent } from '../new-game-modal/new-game-modal.component';

export enum KEY_CODE {
  LEFT_ARROW = 37,
  UP_ARROW = 38,
  RIGHT_ARROW = 39,
  DOWN_ARROW = 40
}
const EMPTY = 0,
  SNAKE = 1,
  APPLE = 2;

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  board: number[][];
  tail: Position[];
  currentPosition = new Position();
  apple = new Position();
  moveSnake: any;
  score = 0;

  lockLeft: boolean;
  lockRight: boolean;
  lockUp: boolean;
  lockDown: boolean;
  interval: any;

  constructor(private modalService: NgbModal) {}

  checkFinish() {
    if (
      this.currentPosition.x < 0 ||
      this.currentPosition.x > 9 ||
      this.currentPosition.y < 0 ||
      this.currentPosition.y > 9 ||
      this.tail.find(
        position =>
          position.x === this.currentPosition.x &&
          position.y === this.currentPosition.y
      )
    ) {
      clearInterval(this.interval);
      const modalRef = this.modalService.open(NewGameModalComponent);
      modalRef.componentInstance.score = this.score;
      modalRef.result.then(result => {
        window.location.reload();
      });
    }
  }

  ngOnInit() {
    this.clearBoard();
    this.initSnake();
    this.addApple();

    this.interval = setInterval(() => {
      this.checkFinish();
      this.lockLeft = false;
      this.lockRight = false;
      this.lockUp = false;
      this.lockDown = false;
      if (this.moveSnake === this.moveUp) {
        this.lockDown = true;
      } else if (this.moveSnake === this.moveDown) {
        this.lockUp = true;
      } else if (this.moveSnake === this.moveRight) {
        this.lockLeft = true;
      } else if (this.moveSnake === this.moveLeft) {
        this.lockRight = true;
      }
      const head = Object.assign({}, this.currentPosition);
      this.moveSnake();
      if (
        this.board[this.currentPosition.x][this.currentPosition.y] === APPLE
      ) {
        this.score++;
        this.board[this.currentPosition.x][this.currentPosition.y] = SNAKE;
        this.tail.push(head);
        this.addApple();
      }
      this.board[this.currentPosition.x][this.currentPosition.y] = SNAKE;

      this.clearBoard();
      this.board[this.currentPosition.x][this.currentPosition.y] = SNAKE;
      this.board[this.apple.x][this.apple.y] = APPLE;

      if (this.tail.length > 0) {
        this.moveTail(head);
      }
      this.checkFinish();
    }, 200);
  }

  initSnake() {
    this.tail = [new Position(6, 5)];
    this.moveSnake = this.moveUp;
    this.currentPosition.x = 5;
    this.currentPosition.y = 5;
    this.board[this.currentPosition.x][this.currentPosition.y] = SNAKE;
  }

  moveTail(head: Position) {
    let tmp;

    for (let i = 0; i < this.tail.length - 1; i++) {
      tmp = this.tail[i + 1];
      this.tail[i].x = tmp.x;
      this.tail[i].y = tmp.y;
    }
    this.tail[this.tail.length - 1].x = head.x;
    this.tail[this.tail.length - 1].y = head.y;

    for (const position of this.tail) {
      this.board[position.x][position.y] = SNAKE;
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.keyCode === KEY_CODE.RIGHT_ARROW && !this.lockRight) {
      this.moveSnake = this.moveRight;
    } else if (event.keyCode === KEY_CODE.LEFT_ARROW && !this.lockLeft) {
      this.moveSnake = this.moveLeft;
    } else if (event.keyCode === KEY_CODE.UP_ARROW && !this.lockUp) {
      this.moveSnake = this.moveUp;
    } else if (event.keyCode === KEY_CODE.DOWN_ARROW && !this.lockDown) {
      this.moveSnake = this.moveDown;
    }
    this.board[this.currentPosition.x][this.currentPosition.y] = SNAKE;
  }

  clearBoard() {
    this.board = [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
  }

  moveLeft = function() {
    this.currentPosition.moveLeft();
  };
  moveRight = function() {
    this.currentPosition.moveRight();
  };

  moveDown = function() {
    this.currentPosition.moveDown();
  };

  moveUp = function() {
    this.currentPosition.moveUp();
  };

  addApple() {
    do {
      this.apple.x = Math.floor(Math.random() * (9 - 0 + 1)) + 0;
      this.apple.y = Math.floor(Math.random() * (9 - 0 + 1)) + 0;
    } while (
      this.currentPosition.x == this.apple.x &&
      this.currentPosition.y == this.apple.y
    );
    this.board[this.apple.x][this.apple.y] = APPLE;
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  }
}
