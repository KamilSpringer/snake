import { Component, OnInit, Input } from '@angular/core';
import { HighScore } from '../models/high-score';
import { HighScoreService } from '../services/high-score.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-new-game-modal',
  templateUrl: './new-game-modal.component.html',
  styleUrls: ['./new-game-modal.component.css']
})
export class NewGameModalComponent implements OnInit {
  @Input() score: number;
  playerName: string;
  scores: HighScore[];
  showButton = true;
  highscoreModal = false;
  constructor(
    private service: HighScoreService,
    public modal: NgbActiveModal
  ) {}

  ngOnInit() {
    this.service.getScores().subscribe(response => {
      this.scores = response;
      if (this.scores.find(x => x.Score < this.score)) {
        this.highscoreModal = true;
      }
    });
  }

  addScore() {
    const playerScore = new HighScore(this.playerName, this.score);
    this.service.saveHighScore(playerScore).subscribe();
    this.showButton = false;
    this.scores.push(playerScore);
    this.modal.close();
  }
}
