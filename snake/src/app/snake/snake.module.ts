import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SnakeRoutingModule } from './snake-routing.module';
import { BoardComponent } from './board/board.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NewGameModalComponent } from './new-game-modal/new-game-modal.component';
import { AddScoreModalComponent } from './add-score-modal/add-score-modal.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [BoardComponent, NewGameModalComponent, AddScoreModalComponent],
  imports: [
    CommonModule,
    SnakeRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule
  ],
  exports: [BoardComponent],
  entryComponents: [NewGameModalComponent]
})
export class SnakeModule {}
