import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HighScore } from '../models/high-score';

const CONTROLLER_URL = 'http://localhost:58973/api/HighestScores';

@Injectable({
  providedIn: 'root'
})
export class HighScoreService {
  constructor(private httpClient: HttpClient) {}

  saveHighScore(score: HighScore) {
    return this.httpClient.post<any>(CONTROLLER_URL, score);
  }

  getScores() {
    return this.httpClient.get<any>(CONTROLLER_URL);
  }
}
