export class HighScore {
  PlayerName: string;
  Score: number;

  constructor(playerName, score) {
    this.PlayerName = playerName;
    this.Score = score;
  }
}
