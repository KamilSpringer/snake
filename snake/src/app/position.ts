export class Position {
  x: number;
  y: number;

  constructor(x?: number, y?: number) {
    this.x = x || 0;
    this.y = y || 0;
  }

  moveRight() {
    this.y += 1;
  }

  moveLeft() {
    this.y -= 1;
  }

  moveUp() {
    this.x -= 1;
  }

  moveDown() {
    this.x += 1;
  }
}
